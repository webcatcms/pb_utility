import os
import sqlite3
from datetime import datetime
class Model(object):
    _instance = None

    def __new__(cls):
        if cls._instance is None:
           cls._instance = super(Model, cls).__new__(cls)
        return cls._instance

    def __init__(self):
        self.settings_conn = sqlite3.connect(os.getcwd() + "\database.db", check_same_thread=False)
        self.setting_cursor = self.settings_conn.cursor()
        self.setting_cursor.execute('SELECT * FROM settings LIMIT 1')
        self.settings = self.setting_cursor.fetchone()

    def get_settings(self):
        self.setting_cursor.execute('SELECT * FROM settings LIMIT 1')
        return self.setting_cursor.fetchone()

    def set_settings(self, data):
        args = (
            data['httpd_port_spinBox'].value(),
            data['range_ip'].text(),
            int(data['zabbix_checkBox'].isChecked()),
            data['server_zabbix_lineEdit'].text(),
            data['host_zabbix_lineEdit'].text(),
            data['key_zabbix_lineEdit'].text(),
            data['time_stop_spinBox'].value()
            )

        sql = ''' UPDATE settings SET 
            httpd_port=?,
            host_terminal=?,
            zabbix_enable=?,
            zabbix_server=?,
            zabbix_host=?,
            zabbix_key=?,
            time_stop=?    
            '''

        self.setting_cursor.execute(sql, args)
        self.settings_conn.commit()

    def set_log(self, data):
        self.setting_cursor.execute("INSERT INTO logs(timestamp , level, event, data) VALUES(?,?,?,?)", data)
        self.settings_conn.commit()

    def get_log(self):
        now = datetime.now()
        date_in = now.strftime("%Y-%m-%d") + r' 00:00:00'
        date_out = now.strftime("%Y-%m-%d") + r' 23:59:59'
        self.setting_cursor.execute('SELECT * FROM logs WHERE timestamp > ? AND timestamp < ?  ', (date_in, date_out,))
        return self.setting_cursor.fetchall()

    def search_log(self, date_in, date_out):
        date_in = date_in.split('.')
        date_out = date_out.split('.')
        date_in = date_in[2] + '-' + date_in[1] + '-' + date_in[0] + r' 00:00:00'
        date_out = date_out[2] + '-' + date_out[1] + '-' + date_out[0] + r' 23:59:59'
        self.setting_cursor.execute('SELECT * FROM logs WHERE timestamp > ? AND timestamp < ?  ', (date_in, date_out,))
        return self.setting_cursor.fetchall()
    def create_index_log(self):
        self.setting_cursor.execute("CREATE INDEX logs_timestamp_IDX ON  logs('timestamp');")
        self.settings_conn.commit()

    def check_index(self):
        self.setting_cursor.execute("select count(*) from sqlite_master where type= 'index' and name ='logs_timestamp_IDX';")
        return self.setting_cursor.fetchone()
    def vacuum(self):
        self.setting_cursor.execute("VACUUM")
        self.settings_conn.commit()

    def delete_log(self, date_in, date_out):
        date_in = date_in.split('.')
        date_out = date_out.split('.')
        date_in = date_in[2] + '-' + date_in[1] + '-' + date_in[0] + r' 00:00:00'
        date_out = date_out[2] + '-' + date_out[1] + '-' + date_out[0] + r' 23:59:59'
        self.setting_cursor.execute('DELETE FROM logs WHERE timestamp > ? AND timestamp < ?', (date_in, date_out,))
        self.settings_conn.commit()
        self.setting_cursor.execute("VACUUM")
        self.settings_conn.commit()

    def range_date(self):
        self.setting_cursor.execute('select min(timestamp) as date_in, max(timestamp) as date_out from logs')
        return self.setting_cursor.fetchone()

    def get_status(self):
        self.setting_cursor.execute('SELECT time_start FROM settings LIMIT 1')
        return self.setting_cursor.fetchone()

    def get_terminal(self):
        self.setting_cursor.execute('SELECT ip, hostname, mac FROM settings LIMIT 1')
        return self.setting_cursor.fetchone()

    def check_update(self):
        try:
            self.get_terminal()
        except:
            self.setting_cursor.execute('ALTER TABLE settings ADD COLUMN ip TEXT')
            self.setting_cursor.execute('ALTER TABLE settings ADD COLUMN hostname TEXT')
            self.setting_cursor.execute('ALTER TABLE settings ADD COLUMN mac TEXT')
            self.settings_conn.commit()

    def set_terminal(self, args):

        sql = ''' UPDATE settings SET 
            httpd_port=?,
            host_terminal=?,
            zabbix_enable=?,
            zabbix_server=?,
            zabbix_host=?,
            zabbix_key=?,
            time_start=?, 
            time_stop=?,   
            ip=?, 
            hostname=?, 
            mac=? '''

        self.setting_cursor.execute(sql, self.get_settings()[1:-3] + args)
        self.settings_conn.commit()