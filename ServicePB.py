import win32event
import win32service
import servicemanager
import win32serviceutil
from sys import argv
from http.server import BaseHTTPRequestHandler, HTTPServer
from websocket import create_connection
import json
from time import sleep
import psutil
from subprocess import *
from datetime import datetime
# from service_model import Model
import threading
import sqlite3
import winreg as reg
from datetime import datetime
import os

class Model(object):

    def __init__(self):
        with reg.OpenKey(reg.HKEY_LOCAL_MACHINE, "SYSTEM\\CurrentControlSet\\Services\\ServicePB") as h:
            path = reg.EnumValue(h, 3)[1].replace("ServicePB.exe", "database.db")
        base = path.strip("\"")
        self.settings_conn = sqlite3.connect(database=base, check_same_thread=False)
        self.setting_cursor = self.settings_conn.cursor()
        self.setting_cursor.execute('SELECT * FROM settings LIMIT 1')
        self.settings = self.setting_cursor.fetchone()

    def get_settings(self):
        self.setting_cursor.execute('SELECT * FROM settings LIMIT 1')
        return self.setting_cursor.fetchone()

    def set_log(self, data):
        self.setting_cursor.execute("INSERT INTO logs(timestamp , level, event, data) VALUES(?,?,?,?)", data)
        self.settings_conn.commit()


    def set_lock(self, lock):
        sql = ''' UPDATE settings SET 
            time_start=?    
            '''

        self.setting_cursor.execute(sql, (lock,))
        self.settings_conn.commit()

    def get_lock(self):
        self.setting_cursor.execute('SELECT time_start FROM settings LIMIT 1')
        return self.setting_cursor.fetchone()


    def clear_log(self):
        self.setting_cursor.execute('DELETE FROM logs  WHERE NOT timestamp LIKE ?', (datetime.now().strftime("%Y-%m-%d") + "%",))
        self.settings_conn.commit()



"""
HTTP Server API
"""


class S(BaseHTTPRequestHandler):

    def kill_driver(self):
        for proc in psutil.process_iter():
            if proc.name() == "genericDriverJsonETH.exe":
                proc.kill()

    def zabbix_sender(self, data):
        model = Model()
        settings = model.get_settings()
        if settings[3]:
            cmd = 'zabbix_sender -vv -z %s -s "%s" -k "%s" -o "%s" ' % (settings[4],
                                                                        settings[5],
                                                                        settings[6],
                                                                        data,)
            Popen(cmd, shell=True, stdout=PIPE, text=True)

            model.set_log((datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "Інформація", "Відправка",
                           "Статус відправлено на zabbix."))

    def _set_response(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def run_method(self, args):
        app = getattr(self, args['method'])
        app(args)

    def PingDevice(self, data):
        status = False
        model = Model()
        try:
            args = {"method": "PingDevice", "step": 0}
            ws = create_connection(r"ws://localhost:3000/echo", timeout=5)
            request = json.dumps(args)
            ws.send(request)
            response = ws.recv()
            self.wfile.write(response.encode('utf-8'))
            ws.close()
            self.zabbix_sender("OK")
            status = True
        except Exception as e:
            if e.args[0] == 'timed out':
                self.wfile.write('{"message":"Термінал зайнятий іншою операцією, спробуйте пізніше."}'.encode("utf-8"))
                self.zabbix_sender('timed out')

                model.set_log((datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "Помилка", "Отримання", str(e)))
                Popen(["sc", "stop", "genericDriverJsonETH"], stdout=PIPE, stderr=PIPE)
                sleep(3)
                self.kill_driver()
                Popen(["sc", "start", "genericDriverJsonETH"], stdout=PIPE, stderr=PIPE)

                model.set_log((datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "Інформація", "Перезапуск",
                               "Служба genericDriverJsonETH."))
            else:
                self.zabbix_sender(10061)

                model.set_log((datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "Помилка", "Отримання", str(e)))
                Popen(["sc", "stop", "genericDriverJsonETH"], stdout=PIPE, stderr=PIPE)
                sleep(3)
                self.kill_driver()
                Popen(["sc", "start", "genericDriverJsonETH"], stdout=PIPE, stderr=PIPE)

                model.set_log((datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "Інформація", "Перезапуск",
                               "Служба genericDriverJsonETH."))
                self.wfile.write(
                    '{"message":"Перепідключіть на терміналі касове суміщення та спробуйте ще раз."}'.encode("utf-8"))
        finally:
            return status

    def TestDevice(self):
        status = False
        model = Model()
        try:
            args = {"method": "PingDevice", "step": 0}
            ws = create_connection(r"ws://localhost:3000/echo", timeout=5)
            request = json.dumps(args)
            ws.send(request)
            ws.recv()
            ws.close()
            self.zabbix_sender("OK")
            status = True
        except Exception as e:
            if e.args[0] == 'timed out':

                model.set_log((datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "Помилка", "Отримання", str(e)))
                Popen(["sc", "stop", "genericDriverJsonETH"], stdout=PIPE, stderr=PIPE)
                sleep(3)
                self.kill_driver()
                Popen(["sc", "start", "genericDriverJsonETH"], stdout=PIPE, stderr=PIPE)

                model.set_log((datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "Інформація", "Перезапуск",
                               "Служба genericDriverJsonETH."))
                sleep(5)
                self.wfile.write(
                    '{"message":"Термінал зайнятий іншою операцією, спробуйте ще раз."}'.encode("utf-8"))
                self.zabbix_sender('timed out')
            else:
                self.zabbix_sender(10061)

                model.set_log((datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "Помилка", "Отримання", str(e)))
                Popen(["sc", "stop", "genericDriverJsonETH"], stdout=PIPE, stderr=PIPE)
                sleep(3)
                self.kill_driver()
                Popen(["sc", "start", "genericDriverJsonETH"], stdout=PIPE, stderr=PIPE)

                model.set_log((datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "Інформація", "Перезапуск",
                               "Служба genericDriverJsonETH."))
                sleep(5)
                self.wfile.write(
                    '{"message":"Перепідключіть на терміналі касове суміщення та спробуйте ще раз."}'.encode("utf-8"))

        finally:
            return status

    def Purchase(self, data):
        if self.TestDevice():
            model = Model()
            model.set_lock(1)
            try:
                args = {"method": "Purchase",
                        "step": 0,
                        "params": {
                            "amount": data['amount'],
                            "discount": "",
                            "merchantId": data['merchantId'],
                            "facepay": "false"
                        }}
                ws = create_connection(r"ws://localhost:3000/echo")
                request = json.dumps(args)

                model.set_log((datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "Інформація", "Відправка", request))
                ws.send(request)
                response = ws.recv()

                model.set_log((datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "Інформація", "Отримання", response))
                self.wfile.write(response.encode('utf-8'))
                ws.close()
                model.set_lock(0)
            except Exception:
                pass

    def Refund(self, data):
        if self.TestDevice():
            model = Model()
            model.set_lock(1)
            try:
                args = {"method": "Refund",
                        "step": 0,
                        "params": {
                            "amount": data['amount'],
                            "discount": "",
                            "merchantId": data['merchantId'],
                            "rrn": data['rrn']
                        }}
                ws = create_connection(r"ws://localhost:3000/echo")
                request = json.dumps(args)

                model.set_log((datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "Інформація", "Відправка", request))
                ws.send(request)
                response = ws.recv()

                model.set_log((datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "Інформація", "Отримання", response))
                self.wfile.write(response.encode('utf-8'))
                ws.close()
                model.set_lock(0)
            except Exception:
                pass

    def ServicePbP(self, data):
        if self.TestDevice():
            model = Model()
            model.set_lock(1)
            try:
                args = {"method": "ServicePbP",
                        "step": 0,
                        "params": {
                            "amount": data['amount'],
                            "amountOfParts": data['amountOfParts'],
                        }}
                ws = create_connection(r"ws://localhost:3000/echo")
                request = json.dumps(args)

                model.set_log((datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "Інформація", "Відправка", request))
                ws.send(request)
                response = ws.recv()

                model.set_log((datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "Інформація", "Отримання", response))
                self.wfile.write(response.encode('utf-8'))
                ws.close()
                model.set_lock(0)
            except Exception:
                pass

    def ServiceRefPbP(self, data):
        if self.TestDevice():
            model = Model()
            model.set_lock(1)
            try:
                args = {"method": "ServiceRefPbP",
                        "step": 0,
                        "params": {
                            "amount": data['amount'],
                            "agreementNum": data['agreementNum'],
                        }}
                ws = create_connection(r"ws://localhost:3000/echo")
                request = json.dumps(args)

                model.set_log((datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "Інформація", "Відправка", request))
                ws.send(request)
                response = ws.recv()

                model.set_log((datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "Інформація", "Отримання", response))
                self.wfile.write(response.encode('utf-8'))
                ws.close()
                model.set_lock(0)
            except Exception:
                pass

    def ServicePartlyRefPbP(self, data):
        if self.TestDevice():
            model = Model()
            model.set_lock(1)
            try:
                args = {"method": "ServicePartlyRefPbP",
                        "step": 0,
                        "params": {
                            "amount": data['amount'],
                            "agreementNum": data['agreementNum'],
                        }}
                ws = create_connection(r"ws://localhost:3000/echo")
                request = json.dumps(args)

                model.set_log((datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "Інформація", "Відправка", request))
                ws.send(request)
                response = ws.recv()

                model.set_log((datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "Інформація", "Отримання", response))
                self.wfile.write(response.encode('utf-8'))
                ws.close()
                model.set_lock(0)
            except Exception:
                pass


    def Audit(self, data):
        if self.TestDevice():
            model = Model()
            model.set_lock(1)
            try:
                args = {"method": "Audit",
                        "step": 0,
                        "params": {
                            "merchantId": data['merchantId'],
                        }}
                ws = create_connection(r"ws://localhost:3000/echo")
                request = json.dumps(args)

                model.set_log((datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "Інформація", "Відправка", request))
                ws.send(request)
                response = ws.recv()

                model.set_log((datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "Інформація", "Отримання", response))
                self.wfile.write(response.encode('utf-8'))
                ws.close()
                model.set_lock(0)
            except Exception:
                pass

    def Verify(self, data):
        if self.TestDevice():
            model = Model()
            model.set_lock(1)
            try:
                args = {"method": "Verify",
                        "step": 0,
                        "params": {
                            "merchantId": data['merchantId'],
                        }}
                ws = create_connection(r"ws://localhost:3000/echo")
                request = json.dumps(args)

                model.set_log((datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "Інформація", "Відправка", request))
                ws.send(request)
                response = ws.recv()

                model.set_log((datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "Інформація", "Отримання", response))
                self.wfile.write(response.encode('utf-8'))
                ws.close()
                model.set_lock(0)
            except Exception:
                pass

    def do_POST(self):
        content_length = int(self.headers['Content-Length'])
        post_data = self.rfile.read(content_length)
        data = json.loads(post_data)
        self._set_response()
        self.run_method(data)


"""
WINDOWS SERVICE
"""


class ServicePB(win32serviceutil.ServiceFramework):
    _svc_name_ = 'ServicePB'
    _svc_display_name_ = 'ServicePB'
    _svc_description_ = 'ServicePB 1.0'

    def __init__(self, args):
        win32serviceutil.ServiceFramework.__init__(self, args)
        self.hWaitStop = win32event.CreateEvent(None, 0, 0, None)
        self.isAlive = True
        self.httpd = None
        self.model = Model()
        self.settings = self.model.get_settings()
        self.httpd = HTTPServer(('', self.settings[1]), S)

    def kill_driver(self):
        for proc in psutil.process_iter():
            if proc.name() == "genericDriverJsonETH.exe":
                proc.kill()

    def test_driver(self):
        model = Model()
        while self.isAlive:
            if not model.get_lock()[0]:
                try:
                    args = {"method": "PingDevice", "step": 0}
                    ws = create_connection(r"ws://localhost:3000/echo", timeout=5)
                    request = json.dumps(args)
                    ws.send(request)
                    ws.recv()
                    self.zabbix_sender('OK')
                    model.set_log(
                        (datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "Інформація", "Перервірка", "Перевірка успішна."))
                    ws.close()
                    sleep(self.settings[8])
                except Exception as e:
                    self.model.set_log((datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "Помилка", "Отримання", str(e)))
                    self.zabbix_sender(e.args[0])
                    Popen(["sc", "stop", "genericDriverJsonETH"], stdout=PIPE, stderr=PIPE)
                    sleep(3)
                    self.kill_driver()
                    Popen(["sc", "start", "genericDriverJsonETH"], stdout=PIPE, stderr=PIPE)
                    sleep(5)
                    model.set_log((datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "Інформація", "Перезапуск",
                                   "Служба genericDriverJsonETH."))

    def ping(self):
        model = Model()

        def indexExists(list, index):
            if 0 <= index < len(list):
                return True
            else:
                return False

        args = ['ping', self.settings[2], '-t']
        p = Popen(args, shell=True, stdout=PIPE, text=True)
        count = 0
        for line in iter(p.stdout):
            line = line.replace("\n", "")
            ls = line.split()
            if indexExists(ls, 7):
                if ls[7][:3] == "TTL":
                    if count > 0:
                        self.zabbix_sender('ping ok')
                        model.set_log((datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "Інформація", "Перервірка",
                                       "Wifi з'єднання відновлено."))

                    count = 0
                else:
                    count += 1
                    if count == 4:
                        self.zabbix_sender('ping lost')
                        model.set_log((datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "Помилка", "Перервірка",
                                       "Wifi з'єднання втрачено."))

            if not self.isAlive:
                break

    def zabbix_sender(self, data):
        model = Model()
        settings = model.get_settings()
        if settings[3]:
            cmd = 'zabbix_sender -vv -z %s -s "%s" -k "%s" -o "%s" ' % (settings[4],
                                                                        settings[5],
                                                                        settings[6],
                                                                        data,)
            Popen(cmd, shell=True, stdout=PIPE, text=True)

            model.set_log((datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "Інформація", "Відправка",
                           "Статус відправлено на zabbix."))

    def clear_log(self):
        while self.isAlive:
            now = datetime.now()
            if now.hour == 12:
                self.model.clear_log()
            sleep(2000)

    def main(self):
        test_driver = threading.Thread(target=self.test_driver, )
        test_driver.start()
        ping = threading.Thread(target=self.ping, )
        ping.start()
        self.httpd.serve_forever()
        while self.isAlive:
            if not test_driver.is_alive():
                test_driver = threading.Thread(target=self.test_driver, )
                test_driver.start()
            sleep(5)

    def SvcDoRun(self):
        self.model.set_lock(0)
        self.model.set_log((datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "Інформація", "Запуск", "Служба ServicePB запущена."))
        self.isAlive = True
        self.main()
        self.ReportServiceStatus(win32service.SERVICE_START_PENDING)
        self.ReportServiceStatus(win32service.SERVICE_RUNNING)
        win32event.WaitForSingleObject(self.hWaitStop, win32event.INFINITE)

    def SvcStop(self):
        model = Model()
        model.set_log(
            (datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "Інформація", "Зупинка", "Служба ServicePB зупинена."))
        self.isAlive = False
        self.httpd.shutdown()
        self.ReportServiceStatus(win32service.SERVICE_STOP_PENDING)
        win32event.SetEvent(self.hWaitStop)


if __name__ == '__main__':
    if len(argv) == 1:
        servicemanager.Initialize()
        servicemanager.PrepareToHostSingle(ServicePB)
        servicemanager.StartServiceCtrlDispatcher()
    else:
        win32serviceutil.HandleCommandLine(ServicePB)
