import sqlite3
import winreg as reg
from datetime import datetime


class Model(object):

    def __init__(self):
        with reg.OpenKey(reg.HKEY_LOCAL_MACHINE, "SYSTEM\\CurrentControlSet\\Services\\ServicePB") as h:
            self.path = reg.EnumValue(h, 3)[1].strip("ServicePB.exe")
        self.settings_conn = sqlite3.connect(self.path + "database.db", check_same_thread = False)
        self.setting_cursor = self.settings_conn.cursor()
        self.setting_cursor.execute('SELECT * FROM settings LIMIT 1')
        self.settings = self.setting_cursor.fetchone()

    def get_settings(self):
        self.setting_cursor.execute('SELECT * FROM settings LIMIT 1')
        return self.setting_cursor.fetchone()

    def set_log(self, data):
        self.setting_cursor.execute("INSERT INTO logs(timestamp , level, event, data) VALUES(?,?,?,?)", data)
        self.settings_conn.commit()


    def set_lock(self, lock):
        sql = ''' UPDATE settings SET 
            time_start=?    
            '''

        self.setting_cursor.execute(sql, (lock,))
        self.settings_conn.commit()

    def get_lock(self):
        self.setting_cursor.execute('SELECT time_start FROM settings LIMIT 1')
        return self.setting_cursor.fetchone()


    def clear_log(self):
        self.setting_cursor.execute('DELETE FROM logs  WHERE NOT timestamp LIKE ?', (datetime.now().strftime("%Y-%m-%d") + "%",))
        self.settings_conn.commit()