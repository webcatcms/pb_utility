import sys
import os
import netifaces
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import QProcess, QSortFilterProxyModel
import psutil
import socket
from PyQt5.QtCore import QObject, QThread, pyqtSignal
from subprocess import *
import json
from websocket import create_connection
from ui_windows import Ui_MainWindow
from model import Model
from time import sleep
from datetime import *
import ipaddress
from getmac import get_mac_address
from _thread import *
from helper import ScannerIp

class Worker(QObject):
    result = pyqtSignal(list)
    finished = pyqtSignal(str)
    pool = list
    thread_status = True

    def run(self):
        socket.setdefaulttimeout(0.0)
        for ip in range(int(self.pool[0]), int(self.pool[1])):
            IP = ipaddress.IPv4Address(ip).compressed

            start_new_thread(self.scan, (IP,))

        while self.thread_status:

            self.thread_status = False
            sleep(60)


        self.finished.emit("Сканування завершене!")

    def scan(self, IP):

        try:
            ip_mac = get_mac_address(ip=IP)
            if not ip_mac == None:
                hostname = socket.gethostbyaddr(IP)
                self.result.emit([hostname[0], IP, ip_mac])
        except Exception:
            pass
        finally:
            exit_thread()
        self.thread_status = True

    def set_pool(self, pool):
        self.pool = pool

class WorkerLog(QObject):
    get_logs = pyqtSignal(str)

    def run(self):
        while True:
            self.get_logs.emit("")
            sleep(2)

class UtilityPB(QtWidgets.QMainWindow):


    request_work = pyqtSignal(list)

    def __init__(self):
        super().__init__()
        self.model = Model()
        if not self.model.check_index()[0]:
            self.model.create_index_log()

        self.model.check_update()
        self.service_name = "ServicePB"
        self.driver_name = "genericDriverJsonETH"
        self.settings = self.model.get_settings()
        self.AppSize = self.geometry()
        self.enable_search = False
        self.initUI()
        self.IsLive = True

        self.new_terminal = None


        self.thread_logs = QThread()
        self.worker_logs = WorkerLog()

        self.worker_logs.moveToThread(self.thread_logs)

        self.thread_logs.started.connect(self.worker_logs.run)
        self.worker_logs.get_logs.connect(self.show_log)
        self.worker_logs.get_logs.connect(self.get_status)
        self.thread_logs.start()


    def initUI(self):
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.setWindowTitle("Utility PB")
        self.setWindowIcon(QtGui.QIcon("privat.ico"))
        self.ui.httpd_port_spinBox.setValue(self.settings[1])

        interfaces = netifaces.interfaces()

        for interface in interfaces:
            addresses = netifaces.ifaddresses(interface).get(netifaces.AF_INET)
            if not addresses[0]['addr'] == '127.0.0.1':
                range_str = addresses[0]['broadcast'].replace('255', '0/24')
                self.ui.range_ip.addItem(range_str)



        self.ui.time_stop_spinBox.setValue(self.settings[8])
        if self.settings[3]:
            self.ui.zabbix_checkBox.setChecked(True)
            self.ui.server_zabbix_lineEdit.setEnabled(True)
            self.ui.server_zabbix_lineEdit.setText(self.settings[4])
            self.ui.host_zabbix_lineEdit.setEnabled(True)
            self.ui.host_zabbix_lineEdit.setText(self.settings[5])
            self.ui.key_zabbix_lineEdit.setEnabled(True)
            self.ui.key_zabbix_lineEdit.setText(self.settings[6])
        else:
            self.ui.zabbix_checkBox.setChecked(False)
            self.ui.server_zabbix_lineEdit.setEnabled(False)
            self.ui.host_zabbix_lineEdit.setEnabled(False)
            self.ui.key_zabbix_lineEdit.setEnabled(False)
        if self.model.get_status()[0]:
            self.ui.status_kasa.setText("Термінал зайнятий.")
            self.ui.status_kasa.setStyleSheet("color: rgb(255, 0, 0);")
        self.ui.search_term_pushButton.clicked.connect(self.search_terminal)
        self.ui.set_ip_pushButton.clicked.connect(self.set_terminal)
        self.ui.zabbix_checkBox.stateChanged.connect(self.ui.change_fields_zabbix)
        self.ui.install_service_button.clicked.connect(self.install)
        self.ui.start_service_button.clicked.connect(self.start)
        self.ui.stop_service_button.clicked.connect(self.stop)
        self.ui.restart_service_button.clicked.connect(self.restart)
        self.ui.ping_button.clicked.connect(self.PingDevice)
        self.ui.ping_icmp_button.clicked.connect(self.ping_icmp)
        self.ui.purchase_button.clicked.connect(self.Purchase)
        self.ui.refund_button.clicked.connect(self.Refund)
        self.ui.audit_button.clicked.connect(self.Audit)
        self.ui.verify_button.clicked.connect(self.Verify)
        self.ui.save_pushButton.clicked.connect(self.save)
        self.ui.cancel_pushButton.clicked.connect(self.close)
        self.ui.refresh_pushButton.clicked.connect(self.refresh_log)
        self.ui.search_pushButton.clicked.connect(self.search)
        self.ui.find.textChanged.connect(self.find)
        self.ui.delete_pushButton.clicked.connect(self.delete_log)
        self.ui.logs_tableWidget.clicked.connect(self.handleSelectionChanged)
        self.ui.scan_logs.clicked.connect(self.handleSelectionChangedScannerLog)
        font = QtGui.QFont()
        font.setFamily("Rockwell")
        font.setPointSize(12)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(50)
        self.ui.result_service_label.setFont(font)
        try:
            date_in, date_out = self.model.range_date()
            time_in = datetime.strptime(date_in, "%Y-%m-%d %H:%M:%S")
            time_out = datetime.strptime(date_out, "%Y-%m-%d %H:%M:%S")
            self.ui.dateEdit_in.setDate(QtCore.QDate(time_in.year, time_in.month, time_in.day))
            self.ui.dateEdit_out.setDate(QtCore.QDate(time_out.year, time_out.month, time_out.day))
        except:
            pass
        self.show_log()
        self.show_scanner_log()
        self.show()

        try:
            self.service = psutil.win_service_get(self.service_name)
        except Exception:
            self.ui.result_service_label.setText("Сервіс\n не встановлений!")
            self.ui.result_service_label.setStyleSheet("color: rgb(255, 0, 0);")
            self.ui.start_service_button.setDisabled(True)
            self.ui.stop_service_button.setDisabled(True)
            self.ui.restart_service_button.setDisabled(True)
            self.service = False
        if self.service:
            self.check_status()
            self.status = True
            self.ui.install_service_button.setText("Видалити")
        else:
            self.status = False
            self.ui.install_service_button.setText("Встановити")

    def resizeEvent(self, event):
        self.AppSize = self.geometry()
        self.ui.groupBox_6.setGeometry(QtCore.QRect(400, 10, self.AppSize.width()-410, self.AppSize.height()-225))
        self.ui.groupBox_7.setGeometry(QtCore.QRect(400, self.AppSize.height() - 215, self.AppSize.width() - 410, 200))
        self.ui.result_text_edit.setGeometry(QtCore.QRect(10, 20, self.AppSize.width()-430, 170))
        self.ui.logs_tableWidget.setGeometry(QtCore.QRect(10, 50, self.AppSize.width()-430, self.AppSize.height()-300))
        self.ui.logs_tableWidget.setColumnWidth(3, self.AppSize.width()-430)
        self.ui.status_kasa_label.setGeometry(QtCore.QRect(self.AppSize.width()-580, 22, 60, 16))
        self.ui.status_kasa.setGeometry(QtCore.QRect(self.AppSize.width()-535, 22, 150, 16))
        QtWidgets.QMainWindow.resizeEvent(self, event)

    def closeEvent(self, a0: QtGui.QCloseEvent) -> None:
        self.IsLive = False

    def handleSelectionChanged(self):

        model = self.ui.logs_tableWidget.selectionModel()
        indexes = model.selectedIndexes()
        # self.ui.result_text_edit.clear()
        html = "<span style='color: black;'>" + indexes[3].data() + "</span>"
        self.ui.result_text_edit.appendHtml(html)

    def handleSelectionChangedScannerLog(self):

        self.ui.set_ip_pushButton.setEnabled(True)
        model = self.ui.scan_logs.selectionModel()
        indexes = model.selectedIndexes()
        self.new_terminal = (indexes[0].data(), indexes[1].data(), indexes[2].data())


    def set_terminal(self):
        self.model.set_terminal(self.new_terminal)
        self.ui.set_ip_pushButton.setEnabled(False)
        self.install()

    def set_result(self, v):
        html = "<span style='color: green;'>"+ "Пристрій : " + v[0] + " - " + v[1] + " - " + v[2]+ "</span>"
        self.ui.result_text_edit.appendHtml(html)

    def get_ip(self, mac):

        args = ['arp', '-a']
        p = Popen(args, stdout=PIPE, text=True).stdout.readlines()

        for l in p:
            line = l.replace("\n", "").split()
            try:
                if line[1] == mac.replace(":", "-"):
                    return line[0]
            except:
                pass

    def check_status(self):
        try:
            status = self.service.status()
            if status == "running":
                self.ui.result_service_label.setText("Сервіс\n Запущено!")
                self.ui.result_service_label.setStyleSheet("color: rgb(0, 170, 0);")
                self.ui.start_service_button.setDisabled(True)
                self.ui.restart_service_button.setDisabled(False)
            elif status == "stopped":
                self.ui.result_service_label.setText("Сервіс\n Зупинений!")
                self.ui.result_service_label.setStyleSheet("color: rgb(255, 0, 0);")
                self.ui.stop_service_button.setDisabled(True)
                self.ui.restart_service_button.setDisabled(True)
        except:
            pass

    def install(self):
        font = QtGui.QFont()
        font.setFamily("Rockwell")
        font.setPointSize(12)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(50)
        self.ui.result_service_label.setFont(font)
        if self.status:
            QProcess.execute("sc", ["stop", self.service_name])
            QProcess.execute("sc", ["delete", self.service_name])
            QProcess.execute("sc", ["stop", self.driver_name])
            QProcess.execute("sc", ["delete", self.driver_name])
            self.status = False
            self.ui.install_service_button.setText("Встановити")
            self.ui.result_service_label.setText("Сервіс\n не встановлений!")
            self.ui.result_service_label.setStyleSheet("color: rgb(255, 0, 0);")
            self.ui.start_service_button.setDisabled(True)
            self.ui.stop_service_button.setDisabled(True)
            self.ui.restart_service_button.setDisabled(True)
        else:
            ip = self.model.get_terminal()[0]
            if ip == None:
                html = "<span style='color: red;'>Термінал не знайдений!</span>"
                self.ui.result_text_edit.appendHtml(html)
            else:
                Popen("sc create {} binPath= \"{} -ip {}\" DisplayName= \"{}\" ".format(self.driver_name,
                                                                                        os.getcwd() + r'\genericDriverJsonETH.exe',
                                                                                        ip, "genericDriverJsonETH"),
                      shell=True, stdout=PIPE)
                Popen("sc create {} binPath= \"{}\" DisplayName= \"{}\" type= own start= auto".format(self.service_name,
                                                                                                      os.getcwd() + r'\ServicePB.exe',
                                                                                                      "PBKASA"),
                      shell=True, stdout=PIPE)

                QProcess.execute("sc", ["description", self.service_name, "API сервер для терміналів ПриватБанк."])
                QProcess.execute("sc", ["description", self.driver_name, "Драйвер для терміналів ПриватБанк."])

                self.status = True
                self.ui.install_service_button.setText("Видалити")
                self.ui.result_service_label.setText("Сервіс\n встановлений!")
                self.ui.result_service_label.setStyleSheet("color: rgb(0, 170, 0);")
                self.ui.start_service_button.setDisabled(False)
                self.ui.stop_service_button.setDisabled(True)
                self.ui.restart_service_button.setDisabled(True)

    def save(self):
        self.model.set_settings(self.ui.__dict__)
        self.settings = self.model.get_settings()

    def start(self):
        font = QtGui.QFont()
        font.setFamily("Rockwell")
        font.setPointSize(12)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(50)
        self.ui.result_service_label.setFont(font)
        QProcess.execute("sc", ["start", self.service_name])
        QProcess.execute("sc", ["start", self.driver_name])
        self.ui.result_service_label.setText("Сервіс\n Запущено!")
        self.ui.result_service_label.setStyleSheet("color: rgb(0, 170, 0);")
        self.ui.start_service_button.setDisabled(True)
        self.ui.stop_service_button.setDisabled(False)
        self.ui.restart_service_button.setDisabled(False)

    def stop(self):
        font = QtGui.QFont()
        font.setFamily("Rockwell")
        font.setPointSize(12)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(50)
        self.ui.result_service_label.setFont(font)
        QProcess.execute("sc", ["stop", self.service_name])
        QProcess.execute("sc", ["stop", self.driver_name])
        self.ui.result_service_label.setText("Сервіс\n Зупинений!")
        self.ui.result_service_label.setStyleSheet("color: rgb(255, 0, 0);")
        self.ui.stop_service_button.setDisabled(True)
        self.ui.start_service_button.setDisabled(False)
        self.ui.restart_service_button.setDisabled(True)

    def restart(self):
        font = QtGui.QFont()
        font.setFamily("Rockwell")
        font.setPointSize(12)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(50)
        self.ui.result_service_label.setFont(font)
        QProcess.execute("sc", ["stop", self.service_name])
        QProcess.execute("sc", ["stop", self.driver_name])

        self.ui.result_service_label.setText("Сервіс\n Зупинений!")
        self.ui.result_service_label.setStyleSheet("color: rgb(255, 0, 0);")
        self.ui.result_service_label.repaint()
        sleep(3)
        QProcess.execute("sc", ["start", self.service_name])
        QProcess.execute("sc", ["start", self.driver_name])

        self.ui.result_service_label.setText("Сервіс\n Запущено!")
        self.ui.result_service_label.setStyleSheet("color: rgb(0, 170, 0);")

    def show_log(self):
        if not self.enable_search:
            self.model_view = QtGui.QStandardItemModel()
            self.model_view.setHorizontalHeaderLabels(["Час", "Рівень", "Подія", "Текст"])
            self.logs = self.model.get_log()
            self.model_view.setRowCount(len(self.logs))
            count = 0
            for log in self.logs:
                self.model_view.setItem(count, 0, QtGui.QStandardItem(log[0]))
                self.model_view.setItem(count, 1, QtGui.QStandardItem(log[1]))
                self.model_view.setItem(count, 2, QtGui.QStandardItem(log[2]))
                self.model_view.setItem(count, 3, QtGui.QStandardItem(log[3]))
                count += 1
            self.proxy = QtCore.QSortFilterProxyModel(self)
            self.proxy.setSourceModel(self.model_view)
            self.ui.logs_tableWidget.setModel(self.proxy)
            self.ui.logs_tableWidget.setAutoScroll(False)
            self.ui.logs_tableWidget.setSortingEnabled(True)
            self.ui.logs_tableWidget.setColumnWidth(0, 130)
            self.ui.logs_tableWidget.setColumnWidth(1, 80)
            self.ui.logs_tableWidget.setColumnWidth(2, 80)
            self.ui.logs_tableWidget.setColumnWidth(3, self.AppSize.width()-430)
            self.ui.logs_tableWidget.verticalHeader().setHidden(True)

    def find(self, text):
        if text:
            self.enable_search = True
        else:
            self.enable_search = False
        # model = self.ui.logs_tableWidget.model()
        self.proxyModel = QSortFilterProxyModel()
        self.proxyModel.setSourceModel(self.model_view)

        self.proxyModel.setFilterKeyColumn(-1)
        self.proxyModel.setFilterFixedString(text)

        self.ui.logs_tableWidget.setModel(self.proxyModel)
        # text = "%Purchase%"
        # model = self.ui.logs_tableWidget.model()
        # count = model.rowCount()
        # start = model.index(0, column)
        # matches = model.match(
        #     start, QtCore.Qt.DisplayRole,
        #     text, count, QtCore.Qt.MatchContains)
        # if matches:
        #     for index in matches:
        #
        #         # index.row(), index.column()
        #         self.ui.logs_tableWidget.selectionModel().select(
        #             index,QtCore.QItemSelectionModel.Select)


    def refresh_log(self):
        self.enable_search = False
        self.show_log()

    def get_status(self):
        if self.model.get_status()[0]:
            self.ui.status_kasa.setText("Термінал зайнятий.")
            self.ui.status_kasa.setStyleSheet("color: rgb(255, 0, 0);")
        else:
            self.ui.status_kasa.setText("Термінал вільний.")
            self.ui.status_kasa.setStyleSheet("color: rgb(0, 170, 0);")

    def search(self):
        self.enable_search = True
        self.model_view = QtGui.QStandardItemModel()
        self.model_view.setHorizontalHeaderLabels(["Час", "Рівень", "Подія", "Текст"])
        self.logs = self.model.search_log(self.ui.dateEdit_in.text(), self.ui.dateEdit_out.text())
        self.model_view.setRowCount(len(self.logs))
        count = 0

        for log in self.logs:
            self.model_view.setItem(count, 0, QtGui.QStandardItem(log[0]))
            self.model_view.setItem(count, 1, QtGui.QStandardItem(log[1]))
            self.model_view.setItem(count, 2, QtGui.QStandardItem(log[2]))
            self.model_view.setItem(count, 3, QtGui.QStandardItem(log[3]))
            count += 1
        self.ui.logs_tableWidget.setModel(self.model_view)
        self.ui.logs_tableWidget.setColumnWidth(0, 130)
        self.ui.logs_tableWidget.setColumnWidth(1, 80)
        self.ui.logs_tableWidget.setColumnWidth(2, 80)
        self.ui.logs_tableWidget.setColumnWidth(3, self.AppSize.width()-430)
        self.ui.logs_tableWidget.verticalHeader().setHidden(True)

    def show_scanner_log(self, result=[]):
        self.model_scan = QtGui.QStandardItemModel()
        self.model_scan.setHorizontalHeaderLabels(["IP", "Hostname", "MAC"])

        self.model_scan.setRowCount(result.__len__())

        if result.__len__() > 0:
            count = 0

            for ip in result:
                self.model_scan.setItem(count, 0, QtGui.QStandardItem(ip[0]))
                self.model_scan.setItem(count, 1, QtGui.QStandardItem(ip[1]))
                self.model_scan.setItem(count, 2, QtGui.QStandardItem(ip[2]))

                count += 1


        self.proxy_scan = QSortFilterProxyModel()
        self.proxy_scan.setSourceModel(self.model_scan)
        self.ui.scan_logs.setModel(self.proxy_scan)
        self.ui.scan_logs.setAutoScroll(True)
        self.ui.scan_logs.setSortingEnabled(True)
        self.ui.scan_logs.setColumnWidth(0, 100)
        self.ui.scan_logs.setColumnWidth(1, 127)
        self.ui.scan_logs.sortByColumn(1, QtCore.Qt.SortOrder.AscendingOrder)
        self.ui.scan_logs.setColumnWidth(2, 130)

        self.ui.scan_logs.verticalHeader().setHidden(True)


    def search_terminal(self):
        result = ScannerIp.start(self.ui.range_ip.currentText(), 2000)
        self.show_scanner_log(result)



    def scan_finish(self, v):
        html = "<b>" + v + "</b>"
        self.ui.result_text_edit.appendHtml(html)
        self.ui.search_term_pushButton.setDisabled(False)

    def delete_log(self):
        self.model.delete_log(self.ui.dateEdit_in.text(), self.ui.dateEdit_out.text())
        self.search()

    def ping_icmp(self):
        ip = self.get_ip(self.settings[2])
        if not ip == None:

            args = ['ping', ip]
            si = STARTUPINFO()
            si.dwFlags |= STARTF_USESHOWWINDOW
            for l in Popen(args, stdout=PIPE, text=True, startupinfo=si).stdout.readlines():
                html = "<span style='color: blue;'>"+l+"</span>"
                self.ui.result_text_edit.appendHtml(html)

    def PingDevice(self):

        try:
            args = {"method": "PingDevice", "step": 0}
            ws = create_connection(r"ws://localhost:3000/echo", timeout=5)
            request = json.dumps(args)
            ws.send(request)
            response = ws.recv()
            html = "<span style='color: blue;'>" + response + "</span>"
            self.ui.result_text_edit.appendHtml(html)
            ws.close()
        except Exception:
            CREATE_NO_WINDOW = 0x08000000
            Popen(["sc", "stop", "genericDriverJsonETH"], stdout=PIPE, stderr=PIPE,creationflags=CREATE_NO_WINDOW)
            sleep(3)
            Popen(["sc", "start", "genericDriverJsonETH"], stdout=PIPE, stderr=PIPE,creationflags=CREATE_NO_WINDOW)
            html = "<span style='color: red;'>Немає зв\'язку з терміналом!!!</span>"
            self.ui.result_text_edit.appendHtml(html)

    def Purchase(self):

        try:
            args = {"method": "Purchase",
                    "step": 0,
                    "params": {
                        "amount": self.ui.sum_lineEdit.text(),
                        "discount": "",
                        "merchantId": "",
                        "facepay": "false"
                    }}
            ws = create_connection(r"ws://localhost:3000/echo")
            request = json.dumps(args)
            ws.send(request)
            response = ws.recv()
            html = "<span style='color: blue;'>" + response + "</span>"
            self.ui.result_text_edit.appendHtml(html)
            ws.close()
        except Exception:
            html = "<span style='color: red;'>Немає зв\'язку з терміналом!!!</span>"
            self.ui.result_text_edit.appendHtml(html)

    def Refund(self):

        try:
            args = {"method": "Refund",
                    "step": 0,
                    "params":{
                        "amount": self.ui.sum_lineEdit.text(),
                        "discount":"",
                        "merchantId":"",
                        "rrn": self.ui.rrn_lineEdit.text()
                    }}
            ws = create_connection(r"ws://localhost:3000/echo")
            request = json.dumps(args)
            ws.send(request)
            response = ws.recv()
            html = "<span style='color: blue;'>" + response + "</span>"
            self.ui.result_text_edit.appendHtml(html)
            ws.close()
        except Exception:
            html = "<span style='color: red;'>Немає зв\'язку з терміналом!!!</span>"
            self.ui.result_text_edit.appendHtml(html)

    def Audit(self):

        try:
            args = {"method": "Audit",
                    "step": 0,
                    "params": {
                        "merchantId": "",
                    }}
            ws = create_connection(r"ws://localhost:3000/echo")
            request = json.dumps(args)
            ws.send(request)
            response = ws.recv()
            html = "<span style='color: blue;'>" + response + "</span>"
            self.ui.result_text_edit.appendHtml(html)
            ws.close()
        except Exception:
            html = "<span style='color: red;'>Немає зв\'язку з терміналом!!!</span>"
            self.ui.result_text_edit.appendHtml(html)

    def Verify(self):

        try:
            args = {"method": "Verify",
                    "step": 0,
                    "params": {
                        "merchantId": "",
                    }}
            ws = create_connection(r"ws://localhost:3000/echo")
            request = json.dumps(args)
            ws.send(request)
            response = ws.recv()
            html = "<span style='color: blue;'>" + response + "</span>"
            self.ui.result_text_edit.appendHtml(html)
            ws.close()

        except Exception:
            html = "<span style='color: red;'>Немає зв\'язку з терміналом!!!</span>"
            self.ui.result_text_edit.appendHtml(html)


STYLE_SHEET = '''
QHeaderView::section {
    background-color: rgb(220, 220, 220);
    color: black;
    height: 25px;
    width: 100%; 
    font: 12px;
}
QTableView::item:selected {
    background-color: rgb(161, 163, 166);
}

'''

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    app.setStyleSheet(STYLE_SHEET)
    ex = UtilityPB()
    sys.exit(app.exec_())