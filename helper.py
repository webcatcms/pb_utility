import asyncio
import ipaddress
import socket
import netifaces
from getmac import get_mac_address


class ScannerIp():

    result = []

    @staticmethod
    async def scan_host(ip, port):
        loop = asyncio.get_event_loop()
        try:
            reader, writer = await asyncio.open_connection(ip, port)
            writer.close()
            await writer.wait_closed()
            hostname = await loop.run_in_executor(None, socket.gethostbyaddr, ip)
            return [ip, hostname[0], get_mac_address(ip=ip)]
        except (socket.gaierror, socket.error, socket.herror):
            pass

    @staticmethod
    async def scan_network(network, port):
        tasks = []
        for ip in ipaddress.IPv4Network(network).hosts():
            tasks.append(ScannerIp.scan_host(str(ip), port))

        ScannerIp.result = await asyncio.gather(*tasks)

    @staticmethod
    def start(network, port):
        asyncio.run(ScannerIp.scan_network(network, port))
        return [i for i in ScannerIp.result if i is not None]

